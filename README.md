# TCP Port Scanner

This scanner is written in Go. It follows the instructions described in [Black Hat Go](https://nostarch.com/blackhatgo). My main goal is to write a simple cli app that I can use quickly and easily to practice cyber security blue team scenarios. However from that I hope to be able to further my understanding of Go while also exploring potentially useful features such as filtering and logging.
